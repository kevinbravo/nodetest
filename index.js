var express = require('express');
var bodyParser = require('body-parser');
var app = express();
//CODE AJOUTÉ
var pausedUsers = {}

console.log('hello user:')
app.use(bodyParser.json());

app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index');
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});


app.post('/facebook/receive/', function(req, res) {
	const dashbot = require('dashbot')(process.env.iTiNyRegd2l6SkQAvCiUaOURBxxz9x124CcnlLbV).facebook;
  dashbot.logIncoming(req.body);
});


app.post('/pause', bodyParser, function (req, res) {
  const userId = req.body.userId
  const paused = req.body.paused
  pausedUsers[userId] = paused
  res.send('ok')
})

app.post('/webhook/', bodyParser, function (req, res) {
  dashbot.logIncoming(req.body);
  if(req.body.entry){
    req.body.entry.forEach(function(entry){
      if(entry.messaging){
        entry.messaging.forEach(function(event){
          var recipientId = event.sender.id;
          if(!pausedUsers[recipientId]){
            //handle message if session is not paused for this userId
            console.log('paused user:' + pausedUsers)
          }
        })
      }
    })
  }
})

console.log('paused user:' + pausedUsers)